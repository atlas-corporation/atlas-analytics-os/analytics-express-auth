# analytics-express-auth

### A component of the Atlas Analytics platform

## About Atlas Analytics

Atlas Analytics is a platform for obtaining and reporting on analytics in the metaverse which rely on the Decentraland protocol. The platform was made open source as part of a [Decentraland grant](https://decentraland.org/governance/proposal/?id=fe85ab06-618d-4181-960d-fc32d5f0a7e1) and this repository constitues on of the microservices that comprises that platform.

The Atlas Analytics stack has the following components:

* `analytics-ingress` : Express application to receive analytics data from in-world. Contains filters and checks for users to apply if desired.

* `atlas-analytics-app`: The front-end, react application users log into to view reports on their analytics. 

* `analytics-query` : The back-end of the analytics reporting application, providing queries into the database of collected data. Written in python.

* **YOU ARE HERE** `analytics-express-auth` : A microservice for validating signatures due to functionality unsupported in python. Written in express.

* `analytics-update` : More back-end code, specifically for large queries that require caching periodically. Written in python.

* `analytics-update-cron` : A node-red application governing the periodic triggering of the `analytics-update` caching microservice.

*  `mongo-change-stream-server` : An event handler, written in express, to handle keeping a current index of active scenes and last updates.

* `mongo-change-stream-server-worlds` : An event handler, written in express, to handle keeping a current index of active scenes and last updates for worlds scenes.

## About analytics-express-auth


This app is a lightweight express microservice whose sole job is to verify a signature passed to it from a front end since python's web3py library does not support the necessary functions to do so and therefore a node/express app is required.

This app needs to be on the same network as the `analytics-query` microservice such that they can communicate with each without hitting the open internet.


## Deploying this Microservice

This microservice can be deployed using the dockerfile present in the repository. Simply run using `docker build -t analytics-express-auth . && docker run analytics-express-auth`. It should probably be run in concert with the `analytics-query` microservice as it is meant to support that. In that case use `docker-compose build && docker-compose up -d` to execute.