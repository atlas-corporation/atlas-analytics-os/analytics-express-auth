const express = require('express');
const bodyParser = require('body-parser');
const { ethers } = require("ethers");

const app = express();
app.use(express.json());

app.post('/recover-message', (req, res) => {
    const domain = {
        name: "Atlas Analytics",
        version: "1",
        chainId: 1,
      };
    
    const types = {
        nonce: [
        { name: "address", type: "string" },
        { name: "nonce", type: "string" },
        ]
    };
    
    console.log('Got body:', req.body);
    const address = req.body.address;
    const nonce = req.body.nonce;
    const signature = req.body.signature;
    const value = {
        address: address,
        nonce: String(nonce),
    };
    
    const verified_address = ethers.utils.verifyTypedData(domain, types, value, signature)
    console.log(verified_address); 
    res.send(verified_address);
});

app.listen(3000, () => {
    console.log('The application is listening on port 3000!');
})
